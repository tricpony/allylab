//
//  ViewController.swift
//  AllyLab
//
//  Created by aarthur on 3/17/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pinWheel: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        imageView.layer.borderWidth = 0.5
        title = "Photo"
        pinWheel.color = .black
    }

    var randomId: Int {
        return Int.random(in: 1..<101)
    }
    
    @IBAction func performLoadPhoto(_ sender: Any) {
        pinWheel.startAnimating()
        let w = Int(imageView.frame.size.width)
        let h = Int(imageView.frame.size.height)
        let urlString = "https://picsum.photos/id/\(randomId)/\(w)/\(h)"
        let _ = ServiceManager.sharedService.startServiceAt(urlString: urlString) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    strongSelf.pinWheel.color = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    strongSelf.pinWheel.stopAnimating()
                    let image = UIImage(data: data)
                    strongSelf.imageView.image = image
                }
            case .failure( _ ):
                DispatchQueue.main.async {
                    strongSelf.pinWheel.color = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    strongSelf.pinWheel.stopAnimating()
                    // Error occurred, alert the user
                    let alertController = UIAlertController(title: "Alert", message: "Service Failed", preferredStyle: .alert)
                    let buttonAction = UIAlertAction(title: "OK", style: .default, handler:nil)
                    alertController.addAction(buttonAction)
                    strongSelf.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
}
