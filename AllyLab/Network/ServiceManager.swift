//
//  ServiceManager.swift
//  JSInjectionLab
//
//  Created by aarthur on 3/3/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import Foundation

struct RCError: Error {
}

struct ServiceManager {
    private let session: URLSession
    init(session: URLSession) {
        self.session = session
    }
    
    //MARK: Singleton

    static var sharedService = ServiceManager(session: URLSession.shared)

    /// Request service at urlString.
    /// - Parameters:
    ///   - urlString: Web address of service.
    ///   - completion: Call back to indicate success or failure associated with payload or error.
    func startServiceAt(urlString: String, completion:@escaping (Swift.Result<Data, Error>)->()) -> URLSessionDataTask? {
        guard let url = URL(string: urlString) else { return nil }
        let request = URLRequest(url: url)
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                completion(.failure(RCError()))
                return
            }
            completion(.success(data))
        }
        task.resume()
        return task
    }
}
