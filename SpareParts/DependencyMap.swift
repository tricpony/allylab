//
//  DependencyMap.swift
//  CartAndCheckOutKitchenSink
//
//  Created by Goulla, Manoj babu - Manoj babu on 06/08/19.
//  Copyright © 2019 Goulla, Manoj babu - Manoj babu. All rights reserved.
//

import Foundation
import AppSpec

/// Registers all dependencies on initialization. Use `resolve(_:)` method to resolve dependencies
class DependencyMap: DependencyMapper {
    init() {
        // Register dependencies
    }
    
    static public func setup() {
        DependencyManager.initialize(dependencyMapper: DependencyMap())
    }
    
    func resolve<Service>(serviceType: Service.Type) -> Service? {
        // Need to create a custom dependency container to register and solve dependencies
        return nil
    }
}
