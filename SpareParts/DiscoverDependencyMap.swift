//
//  DependencyMap.swift
//  Discover
//
//  Created by Arthur, Anthony - Anthony on 10/30/19.
//  Copyright © 2019 Lowe's Home Improvement. All rights reserved.
//

import Foundation
import AppSpec
import LowesCore
import LowesCoreFoundation
import DataModel
import Swinject

public class DiscoverDependencyMap: DependencyMapper, MockDataSupport {
    private let container = Container()

    public init() {
        registerStringFormatterUtility()
        registerPriceDisplayInfo()
        registerBadgesViewManager()
        registerLogger()
        registerRemoteConfig()
        registerRefinement()
        registerRefinementListing()
        registerRefinementValue()
        registerProductCategory()
        #if DEBUG
        registerMockProducts()
        registerMockRefinements()
        registerMockSortChoices()
        #endif
    }

    /// Register class conforming to ASCStringFormatterUtility as a singleton.
    func registerStringFormatterUtility() {
        container.register(ASCStringFormatterUtility.self) { _ in StringFormatterUtility() }.inObjectScope(.container)
    }
    
    /// Register class conforming to ASCPriceDisplayInfo.  To resolve you must pass arguments for displayContainerType, product, & priceDisplayFormatter following the
    /// signature implemented below.
    func registerPriceDisplayInfo() {
        container.register(ASCPriceDisplayInfo.self) { _, displayContainerType, product, priceDisplayFormatter in
            PriceDisplayInfo(displayContainerType: displayContainerType,
                             product: product,
                             priceDisplayFormatter: priceDisplayFormatter)
        }
    }
    
    /// Register class conforming to ASCBadgesViewManager.  To resolve you must pass arguments for upperTopBadge, upperBottomBadge, & lowerBadge following the
    /// signature implemented below.
    func registerBadgesViewManager() {
        container.register(ASCBadgesViewManager.self) { _, upperTopBadge, upperBottomBadge, lowerBadge in
            BadgesViewManager(upperTopBadge: upperTopBadge,
                              upperBottomBadge: upperBottomBadge,
                              lowerBadge: lowerBadge)
        }
    }
    
    /// Register class conforming to ASCProductCategory.
    func registerProductCategory() {
        container.register(ASCProductCategory.self) { _, name, nValue, categoryId, subCategories in
            ProductCategory(name: name,
                            nValue: nValue,
                            categoryId: categoryId,
                            subCategories: subCategories)
        }
    }
    
    /// Register class conforming to ASCRefinement.
    func registerRefinement() {
        container.register(ASCRefinement.self) { _, name, navStateId, multiSelect, refinementValues in
            Refinement(name: name,
                       navStateId: navStateId,
                       multiSelect: multiSelect,
                       refinementValues: refinementValues)
        }
    }

    /// Register class conforming to ASCRefinementListing.
    func registerRefinementListing() {
        container.register(ASCRefinementListing.self) { _, refinement, position in
            RefinementListing(refinement: refinement, position: position)
        }
    }
    
    /// Register class conforming to ASCRefinementListing.
    func registerRefinementValue() {
        container.register(ASCRefinementValue.self) { _, name, navStateId, productCount, isCategory in
            RefinementValue(name: name, navStateId: navStateId, productCount: productCount, isCategory: isCategory)
        }
    }

    /// Register class conforming to ASCLogger as a singleton.
    func registerLogger() {
        container.register(ASCLogger.self) { _ in OSLogger() }.inObjectScope(.container)
    }
    
    /// Register class conforming to BCPAccessor as a singleton.
    func registerRemoteConfig() {
        container.register(BCPAccessor.self) { _ in RemoteConfigAccessor() }.inObjectScope(.container)
    }

    /// Register class conforming to ASCProductList as a singleton.
    func registerMockProducts() {
        container.register([ASCProductListAble].self) { [weak self] _ in
            guard let mockProducts = self?.mockProducts else {
                return [ASCProductListAble]()
            }
            return mockProducts
        }.inObjectScope(.container)
    }

    /// Register mock refinements as a singleton.  These are used as filter choices.
    func registerMockRefinements() {
        container.register([ASCRefinement].self) { [weak self] _ in
            guard let mockRefinements = self?.mockRefinements else {
                return [ASCRefinement]()
            }
            return mockRefinements
        }.inObjectScope(.container)
    }
    
    /// Register mock sort choices as a singleton.  These are used as filter choices.
    func registerMockSortChoices() {
        container.register([SortChoice].self) { [weak self] _ in
            guard let mockSortChoices = self?.mockSortChoices else {
                return [SortChoice]()
            }
            return mockSortChoices
        }.inObjectScope(.container)
    }

    static public func setup() {
        DependencyManager.initialize(dependencyMapper: DiscoverDependencyMap())
    }
    
    /// This method resolves an injected object.
    ///
    /// - Parameters:
    ///   - serviceType: The type of object we want an instance of.
    public func resolve<Service>(serviceType: Service.Type) -> Service? {
        return self.container.resolve(serviceType)
    }
    
    /// This method accepts a two argument option to resolve an injected object.
    ///
    /// - Parameters:
    ///   - serviceType: The type of object we want an instance of.
    ///   - arg1: The first argument needed to initialize an instance of serviceType.
    ///   - arg2: The second argument needed to initialize an instance of serviceType.
    ///   - arg3: The third argument needed to initialize an instance of serviceType.
    /// - Returns: An instance of Service.
    public func resolve<Service, Arg1, Arg2>(
        _ serviceType: Service.Type,
        arguments arg1: Arg1, _ arg2: Arg2
    ) -> Service? {
        return self.container.resolve(serviceType, arguments: arg1, arg2)
    }
    
    /// This method accepts a three argument option to resolve an injected object.
    ///
    /// - Parameters:
    ///   - serviceType: The type of object we want an instance of.
    ///   - arg1: The first argument needed to initialize an instance of serviceType.
    ///   - arg2: The second argument needed to initialize an instance of serviceType.
    ///   - arg3: The third argument needed to initialize an instance of serviceType.
    /// - Returns: An instance of Service.
    public func resolve<Service, Arg1, Arg2, Arg3>(
        _ serviceType: Service.Type,
        arguments arg1: Arg1, _ arg2: Arg2, _ arg3: Arg3
    ) -> Service? {
        return self.container.resolve(serviceType, arguments: arg1, arg2, arg3)
    }

    /// This method accepts a four argument option to resolve an injected object.
    ///
    /// - Parameters:
    ///   - serviceType: The type of object we want an instance of.
    ///   - arg1: The first argument needed to initialize an instance of serviceType.
    ///   - arg2: The second argument needed to initialize an instance of serviceType.
    ///   - arg3: The third argument needed to initialize an instance of serviceType.
    /// - Returns: An instance of Service.
    public func resolve<Service, Arg1, Arg2, Arg3, Arg4>(
        _ serviceType: Service.Type,
        arguments arg1: Arg1, _ arg2: Arg2, _ arg3: Arg3, _ arg4: Arg4
    ) -> Service? {
        return self.container.resolve(serviceType, arguments: arg1, arg2, arg3, arg4)
    }
}
