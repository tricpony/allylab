//
//  DependencyManager.swift
//  AppSpec
//
//  Created by Struzinski, Mark - Mark on 9/26/19.
//  Copyright © 2019 Lowe's Home Improvement. All rights reserved.
//

import Foundation

/// Implement this protocol to provide the dependency map container for dependency lookup
public protocol DependencyMapper {
    /// Resolve type dependency for the given Service Type and return a instance of Service.Type
    /// - Parameter serviceType: Service.Type to resolve dependency
    func resolve<Service>(serviceType: Service.Type) -> Service?
}

/**
    Singleton to resolve dependencies for a given Service.Type
    During applicaiton launch, set the dependencyMapper with a instance of type implementing DependencyMapper protocol
        
    ### Usage Example: ###
    ````
    // Create a DpendencyMap class in Application target implementing DependencyMapper
    class DependencyMap: DependencyMapper {
      init() {
          // Register dependencies
      }
      
      static public func setup() {
          DependencyManager.initialize(dependencyMapper: DependencyMap())
      }
      
      func resolve<Service>(serviceType: Service.Type) -> Service? {
          // If a dependency is registered for serviceType, then return a instance
          return ServiceTypeInstance
      }
    }
 
    // In applicaiton delegate class, on applicaiton launch call DependencyMap setup method to
    // 1. Register dependencies
    // 2. Setup DependencyManager with DependencyMapper instance
   
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        DependencyMap.setup()
        return true
    }
 
    // To resove or lookup dependency using DependencyManager
    
    let sampleClassInstance = DependencyManager.resolve(serviceType: SampleClass.self)
 
    ````
 */
public class DependencyManager {
    static let sharedInstance = DependencyManager()
    
    private var dependencyMapper: DependencyMapper?

    private init() {}
    
    public static func resolve<Service>(serviceType: Service.Type) -> Service? {
        guard let mapper = self.sharedInstance.dependencyMapper else {
            fatalError("Unable to bootstrap dependencies")
        }
        
        return mapper.resolve(serviceType: serviceType)
    }
    
    public static func initialize(dependencyMapper: DependencyMapper) {
        self.sharedInstance.dependencyMapper = dependencyMapper
    }
}
