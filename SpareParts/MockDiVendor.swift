//
//  DIVendor.swift
//  DataModel
//
//  Created by Arthur, Anthony - Anthony on 11/27/19.
//  Copyright © 2019 Arthur, Anthony - Anthony. All rights reserved.
//

import Foundation
import LowesCoreFoundation
import AppSpec

public extension DIVendor {
    /// Dependency injected  object.
    /// - Returns: An injected object conforming to ASCRefinement.
    static func productCategory(name: String,
                                nValue: String,
                                categoryId: Int,
                                subCategories: [ASCProductCategory]?) -> ASCProductCategory {
        guard let diObject = DependencyManager.resolve(ASCProductCategory.self, arguments: name, nValue, categoryId, subCategories) else {
            fatalError("*** Dependency injection failure: \(String(describing: ASCProductCategory.self))")
        }
        return diObject
    }
    
    /// Dependency injected  object.
    /// - Returns: An injected object conforming to ASCRefinement.
    static func refinement(name: String,
                           navStateId: Int,
                           multiSelect: Bool,
                           refinementValues: ASCRefinementValues) -> ASCRefinement {
        guard let diObject = DependencyManager.resolve(ASCRefinement.self, arguments: name, navStateId, multiSelect, refinementValues) else {
            fatalError("*** Dependency injection failure: \(String(describing: ASCRefinement.self))")
        }
        return diObject
    }

    /// Dependency injected  object.
    /// - Returns: An injected object conforming to ASCRefinementListing.
    static func refinementListing(refinement: ASCRefinement, position: Int?) -> ASCRefinementListing {
        guard let diObject = DependencyManager.resolve(ASCRefinementListing.self, arguments: refinement, position) else {
            fatalError("*** Dependency injection failure: \(String(describing: ASCRefinementListing.self))")
        }
        return diObject
    }

    /// Dependency injected  object.
    /// - Returns: An injected object conforming to ASCRefinementValue.
    static func refinementValue(name: String, navStateId: String, productCount: Int?, isCategory: Bool) -> ASCRefinementValue {
        guard let diObject = DependencyManager.resolve(ASCRefinementValue.self, arguments: name, navStateId, productCount, isCategory) else {
            fatalError("*** Dependency injection failure: \(String(describing: ASCRefinementValue.self))")
        }
        return diObject
    }

    /// Dependency injected  object.
    /// - Returns: An injected object conforming to [ASCProductList].
    static var mockProducts: [ASCProductListAble] {
        guard let diObject = DependencyManager.resolve(serviceType: [ASCProductListAble].self) else {
            fatalError("*** Dependency injection failure: \(String(describing: [ASCProductListAble].self))")
        }
        return diObject
    }

    /// Dependency injected  object.
    /// - Returns: An injected object conforming to [ASCRefinement].
    static var mockRefinements: [ASCRefinement] {
        guard let diObject = DependencyManager.resolve(serviceType: [ASCRefinement].self) else {
            fatalError("*** Dependency injection failure: \(String(describing: [ASCRefinement].self))")
        }
        return diObject
    }
    
    /// Dependency injected  object.
    /// - Returns: An injected [SortChoice] object.
    static var mockSortChoices: [SortChoice] {
        guard let diObject = DependencyManager.resolve(serviceType: [SortChoice].self) else {
            fatalError("*** Dependency injection failure: \(String(describing: [SortChoice].self))")
        }
        return diObject
    }
}
